package com.nanchen.compresshelper;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StringUtilTest {

    private String a = "aaaa";
    private String A = "A";
    private String b = "aaaa";
    private String D = "DDDD";
    private String n = "";
    private String e = "abcd";


    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void isEmpty() {
        String ss = "";
        Assert.assertFalse(StringUtil.isEmpty("asd"));
        Assert.assertTrue(StringUtil.isEmpty(ss));
    }

    @Test
    public void isSpace() {
        String sss ="asda";
        Assert.assertTrue(StringUtil.isSpace(""));
        Assert.assertFalse(StringUtil.isSpace(sss));
    }

    @Test
    public void testEquals() {
        String d = "bb";
        String c = "cccc";
        CharSequence obj = "hello";
        CharSequence obj1 = "aaaa";
        Assert.assertTrue(StringUtil.equals(a, b));
        Assert.assertFalse(StringUtil.equals(a, d));
        Assert.assertFalse(StringUtil.equals(a, c));
        Assert.assertTrue(StringUtil.equals(a,obj1));
        Assert.assertFalse(StringUtil.equals(a,obj));
    }

    @Test
    public void equalsIgnoreCase() {
       String c = "abcd";
       Assert.assertTrue(StringUtil.equalsIgnoreCase(a,b));
       Assert.assertFalse(StringUtil.equalsIgnoreCase(a,c));
       Assert.assertFalse(StringUtil.equalsIgnoreCase(a,c));
    }

    @Test
    public void null2Length0() {
        Assert.assertEquals("",StringUtil.null2Length0(n));
        Assert.assertEquals("aaaa",StringUtil.null2Length0(a));
    }

    @Test
    public void length() {
        Assert.assertEquals(0,StringUtil.length(n));
        Assert.assertEquals(4,StringUtil.length(a));
    }

    @Test
    public void upperFirstLetter() {
        Assert.assertEquals("",StringUtil.upperFirstLetter(n));
        Assert.assertEquals("Aaaa",StringUtil.upperFirstLetter(a));
    }

    @Test
    public void lowerFirstLetter() {
        Assert.assertEquals("",StringUtil.lowerFirstLetter(n));
        Assert.assertEquals("dDDD",StringUtil.lowerFirstLetter(D));
    }

    @Test
    public void reverse() {
      Assert.assertEquals("A",StringUtil.reverse(A));
      Assert.assertEquals("dcba",StringUtil.reverse(e));
    }

    @Test
    public void toDBC() {
        String fullString  = "全；角";
        Assert.assertEquals("",StringUtil.toDBC(n));
        Assert.assertEquals("全;角",StringUtil.toDBC(fullString));
    }

    @Test
    public void toSBC() {
        String halfString  = "半;角";
        Assert.assertEquals("",StringUtil.toDBC(n));
        Assert.assertEquals("半；角",StringUtil.toSBC(halfString));
    }
}