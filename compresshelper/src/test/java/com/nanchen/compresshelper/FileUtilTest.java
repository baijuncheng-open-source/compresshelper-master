package com.nanchen.compresshelper;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.app.Context;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.*;
import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({HiLogLabel.class,FileUtil.class,DataAbilityHelper.class,HiLog.class})
public class FileUtilTest {

    private String filePath;
    private String filePaths;
    private String fileName;
    private Random random;
    private String s;
    private InputStream in;
    private OutputStream out;
    private  File file;
    private Context context;
    private Uri uri;
    private DataAbilityHelper helper;
    @Before
    public void setUp() throws Exception {
        helper = mock(DataAbilityHelper.class);
        context = mock(Context.class);
        uri = mock(Uri.class);
        file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isFile()).thenReturn(true);
        when(file.getParent()).thenReturn("D:/test/est/");
        when(file.getName()).thenReturn("worktojs");
        filePath = "";
        filePaths = "D:/worktest/";
        fileName = "worktoj";
        random  = new Random();
        s = String.valueOf(random.nextInt());
        in = mock(InputStream.class);
        out = mock(OutputStream.class);
        HiLogLabel hiLogLabel = mock(HiLogLabel.class);
        try {
            whenNew(HiLogLabel.class).withAnyArguments().thenReturn(hiLogLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getFileByPath() {
        File file = new File(filePaths);
        Assert.assertNull(FileUtil.getFileByPath(filePath));
        Assert.assertEquals(file,FileUtil.getFileByPath(filePaths));
    }

    @Test
    public void FilePathisFileExists() {
        Assert.assertFalse(FileUtil.isFileExists(filePaths));
    }

    @Test
    public void FileIsFileExists() {
        Assert.assertTrue(FileUtil.isFileExists(file));
        when(file.exists()).thenReturn(false);
        Assert.assertFalse(FileUtil.isFileExists(file));
    }

    @Test
    public void filePathRename() {
        Assert.assertFalse(FileUtil.rename("D:/test","aaa"));
    }

    @Test
    public void fileRename() {
        Assert.assertFalse(FileUtil.rename("dD:/add/",s));
        Assert.assertFalse(FileUtil.rename(file,s));
        Assert.assertFalse(FileUtil.rename(file,""));
        Assert.assertTrue(FileUtil.rename(file,"worktojs"));
        Assert.assertFalse(FileUtil.rename(file,"asd"));
        when(file.exists()).thenReturn(false);
        Assert.assertFalse(FileUtil.rename(file,s));
    }

    @Test
    public void isDir() {
      Assert.assertFalse(FileUtil.isDir(filePath));
        try {
            PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(file);
            when(file.isDirectory()).thenReturn(true);
            Assert.assertTrue(FileUtil.isDir(filePaths));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testIsDir() {
        Assert.assertFalse(FileUtil.isDir(file));
        when(file.isDirectory()).thenReturn(true);
        Assert.assertTrue(FileUtil.isDir(file));
    }

    @Test
    public void isFile() {
      Assert.assertFalse(FileUtil.isFile(filePaths));
    }

    @Test
    public void testIsFile() {
        File file1 = mock(File.class);
        Assert.assertTrue(FileUtil.isFile(file));
        Assert.assertFalse(FileUtil.isFile(file1));
    }

    @Test
    public void renameFile() {
        try {
            PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(file);
            Assert.assertEquals(file,FileUtil.renameFile(file,"newname"));
            File file1 = mock(File.class);
            when(file1.exists()).thenReturn(true);
            when(file1.isFile()).thenReturn(true);
            when(file1.getParent()).thenReturn("D:/test/est/");
            when(file1.getName()).thenReturn("worktojs");
            when(file1.isDirectory()).thenReturn(true);
            when(file1.equals(file)).thenReturn(false);
            Assert.assertEquals(file,FileUtil.renameFile(file,"newmane"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getTempFile() {
        PowerMockito.mockStatic(DataAbilityHelper.class);
        when(DataAbilityHelper.creator(any(Context.class))).thenAnswer(new Answer<DataAbilityHelper>() {
            @Override
            public DataAbilityHelper answer(InvocationOnMock invocationOnMock) throws Throwable {
                return helper ;
            }
        });
        try {
            when(helper.obtainInputStream(any())).thenReturn(in);
            when(uri.getDecodedPath()).thenReturn("D:\\Users\\jh\\AppData\\Local\\Temp\\d:test\\test");
            when(uri.getScheme()).thenReturn("ability");
            PowerMockito.mockStatic(HiLog.class);
            when(HiLog.debug(any(),anyString(),anyString())).thenReturn(1);
            when(in.read(any())).thenReturn(2).thenReturn(4).thenReturn(-1);
            File s = FileUtil.getTempFile(context,uri);
            Assert.assertNotNull(s);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void splitFileName() {
        File files = mock(File.class);
        when(files.getName()).thenReturn("name");
        String[] n = new String[]{"name",""};
        Assert.assertArrayEquals(n,FileUtil.splitFileName(files.getName()));
        File file1 = mock(File.class);
        when(file1.getName()).thenReturn("");
        String[] m = new String[]{"",""};
        Assert.assertArrayEquals(m,FileUtil.splitFileName(file1.getName()));
    }

    @Test
    public void getFileName() {
        when(uri.getScheme()).thenReturn("www");
        when(uri.getDecodedPath()).thenReturn("");
        Assert.assertEquals("",FileUtil.getFileName(context,uri));
        when(uri.getDecodedPath()).thenReturn("bjch");
        Assert.assertEquals("bjch",FileUtil.getFileName(context,uri));
        when(uri.getScheme()).thenReturn("dataability");
        try {
            mockStatic(DataAbilityHelper.class);
            when(DataAbilityHelper.creator(any(Context.class))).thenAnswer(new Answer<DataAbilityHelper>() {
                @Override
                public DataAbilityHelper answer(InvocationOnMock invocationOnMock) throws Throwable {
                    return helper ;
                }
            });
            when(helper.query(any(),any(),any())).thenReturn(null);
            Assert.assertEquals("bjch",FileUtil.getFileName(context,uri));
            ResultSet resultSet = mock(ResultSet.class);
            when(helper.query(any(),any(),any())).thenReturn(resultSet);
            Assert.assertEquals("bjch",FileUtil.getFileName(context,uri));
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getRealPathFromURI() {
       mockStatic(DataAbilityHelper.class);
       when(DataAbilityHelper.creator(any(Context.class))).thenAnswer(new Answer<DataAbilityHelper>() {
           @Override
           public DataAbilityHelper answer(InvocationOnMock invocationOnMock) throws Throwable {
               return helper ;
           }
       });
       when(uri.getDecodedPath()).thenReturn("cfpl");
       when(uri.getScheme()).thenReturn("cccc");
       Assert.assertEquals("cfpl", FileUtil.getRealPathFromURI(context,uri));
        try {
            ResultSet resultSet = mock(ResultSet.class);
            when(helper.query(any(),any(),any())).thenReturn(resultSet);
            Assert.assertEquals("cfpl",FileUtil.getRealPathFromURI(context,uri));
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void copy() {
        try {
            when(in.read(any())).thenReturn(Integer.MAX_VALUE)
                    .thenReturn(4)
                    .thenReturn(-1);
            Assert.assertEquals(-1,FileUtil.copy(in,out));
            when(in.read(any())).thenReturn(2)
                    .thenReturn(4)
                    .thenReturn(-1);
            Assert.assertEquals(6,FileUtil.copy(in,out));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testCopyLarge() {
        byte [] b = new byte[2];
        try {
            when(in.read(any())).thenReturn(2).thenReturn(4).thenReturn(-1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Assert.assertEquals(6,FileUtil.copyLarge(in,out,b));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}