package com.nanchen.compresshelper;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageInfo;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.utils.net.Uri;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static ohos.media.image.common.PixelFormat.ARGB_8888;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.whenNew;


@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor("com.nanchen.compresshelper.FileUtil")
@PrepareForTest({FileUtil.class, ImageSource.class, BitmapUtil.class, TextTool.class, HiLogLabel.class,DataAbilityHelper.class,PixelMap.class})
public class BitmapUtilTest {

    private Context context;
    private Uri uri;
    private ImageSource.DecodingOptions options;
    private ImageSource imageSource;

    @Before
    public void setUp() throws Exception {
        context = mock(Context.class);
        uri = mock(Uri.class);
        options = mock(ImageSource.DecodingOptions.class);
        mockStatic(ImageSource.class);
        imageSource = mock(ImageSource.class);
        when(ImageSource.create(any(String.class), any())).thenAnswer(new Answer<ImageSource>() {
            @Override
            public ImageSource answer(InvocationOnMock invocationOnMock) throws Throwable {
                return imageSource;
            }
        });

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getScaledBitmap() {
        String filePath = "D:student/aor";
        DataAbilityHelper helper = mock(DataAbilityHelper.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        PowerMockito.when(DataAbilityHelper.creator(any(Context.class))).thenReturn(helper);
        PowerMockito.when(uri.getDecodedPath()).thenReturn("cfpl");
        PowerMockito.when(uri.getScheme()).thenReturn("cccc");
        mockStatic(FileUtil.class);
        when(FileUtil.getRealPathFromURI(any(Context.class),any(Uri.class))).thenReturn(filePath);
        ImageSource imageSource = mock(ImageSource.class);
        PowerMockito.mockStatic(ImageSource.class);
        when(ImageSource.create(any(String.class),any())).thenReturn(imageSource);
        ImageInfo imageInfo = mock(ImageInfo.class);
        when(imageSource.getImageInfo()).thenReturn(imageInfo);
        Size size = mock(Size.class);
        Whitebox.setInternalState(imageInfo,"size",size);
        options.desiredSize = size;
        PixelMap pixelMap = mock(PixelMap.class);
        float maxWidth = 100f;
        float maxHeight = 100f;
        PixelFormat bitmapConfig = ARGB_8888;
        try {
            PowerMockito.whenNew(ImageSource.DecodingOptions.class).withNoArguments().thenReturn(options);
            PixelMap pixelMap1 = BitmapUtil.getScaledBitmap(context, uri, maxWidth, maxHeight, bitmapConfig);
            Assert.assertEquals(null, pixelMap1);
            size.height = 240;
            size.width = 120;
            Assert.assertEquals(null, pixelMap1);;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test_getScaledBitmap_with_size() {
        String filePath = "D:student/aor";
        DataAbilityHelper helper = mock(DataAbilityHelper.class);
        PowerMockito.mockStatic(DataAbilityHelper.class);
        PowerMockito.when(DataAbilityHelper.creator(any(Context.class))).thenAnswer(new Answer<DataAbilityHelper>() {
            @Override
            public DataAbilityHelper answer(InvocationOnMock invocationOnMock) throws Throwable {
                return helper ;
            }
        });
        PowerMockito.when(uri.getDecodedPath()).thenReturn("cfpl");
        PowerMockito.when(uri.getScheme()).thenReturn("cccc");
        PowerMockito.mockStatic(FileUtil.class);
        when(FileUtil.getRealPathFromURI(any(Context.class),any(Uri.class))).thenReturn(filePath);
        ImageSource imageSource = mock(ImageSource.class);
        PowerMockito.mockStatic(ImageSource.class);
        when(ImageSource.create(any(String.class),any())).thenReturn(imageSource);
        ImageInfo imageInfo = mock(ImageInfo.class);
        when(imageSource.getImageInfo()).thenReturn(imageInfo);
        Size size = mock(Size.class);
        Whitebox.setInternalState(imageInfo,"size",size);
        options.desiredSize = size;
        size.height = 240;
        size.width = 120;
        PixelMap pixelMap = mock(PixelMap.class);
        float maxWidth = 100f;
        float maxHeight = 100f;
        PixelFormat bitmapConfig = ARGB_8888;
        try {
            PowerMockito.whenNew(ImageSource.DecodingOptions.class).withNoArguments().thenReturn(options);
            PixelMap.InitializationOptions pOptions = mock(PixelMap.InitializationOptions.class);
            PowerMockito.whenNew(PixelMap.InitializationOptions.class).withNoArguments().thenReturn(pOptions);
            PowerMockito.whenNew(Size.class).withAnyArguments().thenReturn(size);
            PowerMockito.mockStatic(PixelMap.class);
            PixelMap pixelMaps = mock(PixelMap.class);
            when(PixelMap.create(any())).thenReturn(pixelMaps);
            PowerMockito.whenNew(Matrix.class).withNoArguments().thenReturn(mock(Matrix.class));
            PowerMockito.whenNew(Canvas.class).withNoArguments().thenReturn(mock(Canvas.class));
            PowerMockito.whenNew(Texture.class).withAnyArguments().thenReturn(mock(Texture.class));
            PowerMockito.whenNew(PixelMapHolder.class).withAnyArguments().thenReturn(mock(PixelMapHolder.class));
            whenNew(Paint.class).withNoArguments().thenReturn(mock(Paint.class));
            PixelMap pixelMap1 = BitmapUtil.getScaledBitmap(context, uri, maxWidth, maxHeight, bitmapConfig);
            Assert.assertEquals(pixelMaps, pixelMap1);;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Test
    public void compressImage() {
        float maxWidth = 100f;
        float maxHeight = 120f;
        String compressFormat = "compressFormat";
        PixelFormat bitmapConfig = ARGB_8888;
        int quality = 80;
        String parentPath = "D:/test/es/";
        String prefix = "prefix";
        String filename = "test\\es\\name.jpg";
        String extension = "extension";
        mockStatic(TextTool.class);
        when(TextTool.isNullOrEmpty(any())).thenReturn(false);
        File file=mock(File.class);
        try {
            when(file.getCanonicalPath()).thenReturn("D:\\");
            whenNew(File.class).withAnyArguments().thenReturn(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mockStatic(FileUtil.class);
        try {
            whenNew(ImageSource.DecodingOptions.class).withNoArguments().thenReturn(options);
            ImageSource imageSource = mock(ImageSource.class);
            PowerMockito.mockStatic(ImageSource.class);
            when(ImageSource.create(anyString(),any())).thenReturn(imageSource);
            ImageInfo imageInfo = mock(ImageInfo.class);
            when(imageSource.getImageInfo()).thenReturn(imageInfo);
            Size size = mock(Size.class);
            Whitebox.setInternalState(imageInfo,"size",size);
            options.desiredSize = size;
            String filePath = "filePath";
            when(FileUtil.getRealPathFromURI(any(),any())).thenReturn(filePath);
            File files = BitmapUtil.compressImage(context, uri, maxWidth, maxHeight, compressFormat, bitmapConfig, quality, parentPath, prefix, filename);
            Assert.assertEquals(file,files);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void generateFilePath() {
        String parentPath = "D:/test/es/";
        String prefix = "prefix";
        String fileName = "name";
        String extension = "";
        File file = mock(File.class);
        mockStatic(TextTool.class);
        when(TextTool.isNullOrEmpty(any())).thenReturn(false);
        file.exists();
        Class<?> clazz = null;
        try {
            clazz = Class.forName("com.nanchen.compresshelper.BitmapUtil");
            Method method = clazz.getDeclaredMethod("generateFilePath", Context.class, String.class, Uri.class, String.class, String.class, String.class);
            method.setAccessible(true);
            String s = (String) method.invoke(null, context, parentPath, uri, extension, prefix, fileName);
            String result = "D:\\test\\es\\name.jpg";
            Assert.assertEquals(result, s);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void calculateInSampleSize() {
        Size size = mock(Size.class);
        options.desiredSize = size;
        size.width = 60;
        size.height = 60;
        try {
            Class<?> clazz = Class.forName("com.nanchen.compresshelper.BitmapUtil");
            Method method = clazz.getDeclaredMethod("calculateInSampleSize", ImageSource.DecodingOptions.class, int.class, int.class);
            method.setAccessible(true);
            int i = (int) method.invoke(null, options, 40, 40);
            int ii = (int) method.invoke(null, options, 50, 50);
            int iii = (int) method.invoke(null, options, 80, 80);
            Assert.assertEquals(2, i);
            Assert.assertEquals(1, ii);
            Assert.assertEquals(1, iii);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}