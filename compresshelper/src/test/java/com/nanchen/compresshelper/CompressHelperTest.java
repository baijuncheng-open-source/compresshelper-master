package com.nanchen.compresshelper;

import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.utils.net.Uri;
import org.junit.*;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.File;
import java.lang.reflect.Field;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressStaticInitializationFor("com.nanchen.compresshelper.FileUtil")
@RunWith(PowerMockRunner.class)
@PrepareForTest({Uri.class, CompressHelper.class, TextTool.class, BitmapUtil.class,FileUtil.class})
public class CompressHelperTest {

    private Context context;
    private Uri uri;
    private float maxWidth;
    private float maxHeight;

    @Before
    public void setUp() throws Exception {
        maxWidth = 100f;
        maxHeight = 120f;
        context = mock(Context.class);
        File file = mock(File.class);
        when(file.getPath()).thenReturn("D:");
        when(context.getCacheDir()).thenReturn(file);
        uri = mock(Uri.class);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getDefault() {
        File file = mock(File.class);
        String path = "D/yyyy/";
        when(context.getCacheDir()).thenReturn(file);
        when(context.getCacheDir().getPath()).thenReturn(path);
        Assert.assertNotNull(CompressHelper.getDefault(context));
        try {
            Field field = CompressHelper.class.getDeclaredField("INSTANCE");
            field.setAccessible(true);
            field.set(null, null);
            Assert.assertNotNull(CompressHelper.getDefault(context));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void compressToFile() {
        File file = mock(File.class);
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.when(Uri.getUriFromFile(any())).thenReturn(uri);
        PowerMockito.mockStatic(BitmapUtil.class);
        File files = CompressHelper.getDefault(context).compressToFile(file);
        Assert.assertEquals(null, files);
    }

    @Ignore
    @Test
    public void compressToBitmap() throws Exception {
        PowerMockito.mockStatic(FileUtil.class);
        when(FileUtil.getRealPathFromURI(any(),any())).thenReturn("path");
        PowerMockito.mockStatic(Uri.class);
        PowerMockito.when(Uri.getUriFromFile(any())).thenReturn(uri);
        PowerMockito.mockStatic(TextTool.class);
        CompressHelper CompressHelper = mock(CompressHelper.class);
        PowerMockito.whenNew(CompressHelper.class).withAnyArguments().thenReturn(CompressHelper);
        File file = mock(File.class);
        PixelMap pixelmap = CompressHelper.getDefault(context).compressToBitmap(file);
        Assert.assertEquals(null, pixelmap);
    }

    @Test
    public void setMaxWidth() {
        float maxWidth = 100f;
        CompressHelper.Builder builder = new CompressHelper.Builder(context);
        builder.setMaxWidth(maxWidth);
        CompressHelper helper = Whitebox.getInternalState(builder, "mCompressHelper");
        float value = Whitebox.getInternalState(helper, "maxWidth");
        Assert.assertEquals(maxWidth, value, 0);
    }

    @Test
    public void setMaxHeight() {
        float maxHeight = 100f;
        CompressHelper.Builder builder = new CompressHelper.Builder(context);
        builder.setMaxHeight(maxHeight);
        CompressHelper helper = Whitebox.getInternalState(builder, "mCompressHelper");
        float value = Whitebox.getInternalState(helper, "maxHeight");
        Assert.assertEquals(maxHeight, value, 0);
    }

    @Test
    public void setCompressFormat() {
        String compressFormat = "compressFormat";
        CompressHelper.Builder builder = new CompressHelper.Builder(context);
        builder.setCompressFormat(compressFormat);
        CompressHelper helper = Whitebox.getInternalState(builder, "mCompressHelper");
        String value = Whitebox.getInternalState(helper, "compressFormat");
        Assert.assertEquals(compressFormat, value);
    }

    @Test
    public void setBitmapConfig() {
        PixelFormat bitmapConfig = PixelFormat.RGB_565;
        CompressHelper.Builder builder = new CompressHelper.Builder(context);
        builder.setBitmapConfig(bitmapConfig);
        CompressHelper helper = Whitebox.getInternalState(builder, "mCompressHelper");
        PixelFormat value = Whitebox.getInternalState(helper, "bitmapConfig");
        Assert.assertEquals(bitmapConfig, value);
    }

    @Test
    public void setQuality() {
        int quality = 10;
        CompressHelper.Builder builder = new CompressHelper.Builder(context);
        builder.setQuality(quality);
        CompressHelper helper = Whitebox.getInternalState(builder, "mCompressHelper");
        int value = Whitebox.getInternalState(helper, "quality");
        Assert.assertEquals(quality, value);
    }

    @Test
    public void setDestinationDirectoryPath() {
        String compressFormat = "compressFormat";
        CompressHelper.Builder builder = new CompressHelper.Builder(context);
        builder.setDestinationDirectoryPath(compressFormat);
        CompressHelper helper = Whitebox.getInternalState(builder, "mCompressHelper");
        String value = Whitebox.getInternalState(helper, "destinationDirectoryPath");
        Assert.assertEquals(compressFormat, value);
    }

    @Test
    public void setFileNamePrefix() {
        String compressFormat = "compressFormat";
        CompressHelper.Builder builder = new CompressHelper.Builder(context);
        builder.setFileNamePrefix(compressFormat);
        CompressHelper helper = Whitebox.getInternalState(builder, "mCompressHelper");
        String value = Whitebox.getInternalState(helper, "fileNamePrefix");
        Assert.assertEquals(compressFormat, value);
    }

    @Test
    public void setFileName() {
        String compressFormat = "compressFormat";
        CompressHelper.Builder builder = new CompressHelper.Builder(context);
        builder.setFileName(compressFormat);
        CompressHelper helper = Whitebox.getInternalState(builder, "mCompressHelper");
        String value = Whitebox.getInternalState(helper, "fileName");
        Assert.assertEquals(compressFormat, value);
    }

    @Test
    public void build() {
        CompressHelper.Builder builder = new CompressHelper.Builder(context);
        Assert.assertNotNull(builder.build());
    }
}