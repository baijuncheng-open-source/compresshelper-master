package com.nanchen.compresshelper;

import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageInfo;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ImageUtil.class,ImageSource.class,PixelMap.class})
public class ImageUtilTest {
    private ImageSource.DecodingOptions options;
    private ImageSource source;
    private  Class<?> threadClazz;
    @Before
    public void setUp() throws Exception {
        options = mock(ImageSource.DecodingOptions.class);
        source = mock(ImageSource.class);
        threadClazz = Class.forName("com.nanchen.compresshelper.ImageUtil");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void calculateInSampleSizePathName() {
        Size size = mock(Size.class);
        size.width = 6;
        size.height = 6;
        options.desiredSize = size;
        try {
            Class<?> threadClazz = Class.forName("com.nanchen.compresshelper.ImageUtil");
            Method method = threadClazz.getDeclaredMethod("calculateInSampleSize",
                    ImageSource.DecodingOptions.class, String.class,
                    int.class, int.class);
            method.setAccessible(true);
            int i = (int) method.invoke(null, options, "asda", 7, 7);
            int ii = (int) method.invoke(null, options, "asda", 2, 2);
            Assert.assertEquals(1, i);
            Assert.assertEquals(2, ii);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void calculateInSampleSize() {
        Size size = mock(Size.class);
        size.width = 6;
        size.height = 6;
        options.desiredSize = size;
        try {
            Method method = threadClazz.getDeclaredMethod("calculateInSampleSize", ImageSource.DecodingOptions.class,
                    int.class, int.class);
            method.setAccessible(true);
            int i = (int) method.invoke(null, options,  7, 7);
            int ii = (int) method.invoke(null, options, 2, 2);
            Assert.assertEquals(1, i);
            Assert.assertEquals(2, ii);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void decodeSampledBitmapFromResource() {
        try {
            ResourceManager resourceManager = mock(ResourceManager.class);
            int reqWidth = 30;
            int reqHeight = 30;
            PowerMockito.whenNew(ImageSource.DecodingOptions.class).withAnyArguments().thenReturn(options);
            Resource resource = mock(Resource.class);
            when(resourceManager.getResource(any(int.class))).thenReturn(resource);
            ImageSource imageSource = mock(ImageSource.class);
            PowerMockito.mockStatic(ImageSource.class);
            when(ImageSource.create(any(InputStream.class),any())).thenReturn(imageSource);
            ImageInfo imageInfo = mock(ImageInfo.class);
            when(imageSource.getImageInfo()).thenReturn(imageInfo);
            Size size = mock(Size.class);
            Whitebox.setInternalState(imageInfo,"size",size);
            options.desiredSize = size;
            PixelMap src = mock(PixelMap.class);
            Rect rect =mock(Rect.class);
            PowerMockito.whenNew(Rect.class).withAnyArguments().thenReturn(rect);
            PowerMockito.mockStatic(PixelMap.class);
            when(PixelMap.create(any(),any(),any())).thenAnswer(new Answer<PixelMap>() {
                @Override
                public PixelMap answer(InvocationOnMock invocationOnMock) throws Throwable {
                    return src;
                }
            });
            int resId= 1;
            PixelMap p = ImageUtil.decodeSampledBitmapFromResource(resourceManager,resId,reqWidth,reqHeight);
            Assert.assertEquals(null,p);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createScaleBitmap() {
        PixelMap src = mock(PixelMap.class);
        PixelMap dst = mock(PixelMap.class);
        PixelMap i ,ii,iii;
        Method method = null;
        try {
            method = threadClazz.getDeclaredMethod("createScaleBitmap", PixelMap.class,int.class,
                    int.class, int.class);
            method.setAccessible(true);
            i = (PixelMap) method.invoke(null, src,  7, 7,2);
            ii = (PixelMap) method.invoke(null, null,  7, 7,3);
            Assert.assertSame(src,i);
            Assert.assertEquals(null,ii);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void decodeSampledBitmapFromFile() {
        try {
            PowerMockito.whenNew(ImageSource.DecodingOptions.class).withAnyArguments().thenReturn(options);
            PowerMockito.mockStatic(ImageSource.class);
            when(ImageSource.create(any(String.class),any())).thenAnswer(new Answer<ImageSource>() {
                @Override
                public ImageSource answer(InvocationOnMock invocationOnMock) throws Throwable {
                    return source;
                }
            });
            ImageSource imageSource = mock(ImageSource.class);
            PowerMockito.mockStatic(ImageSource.class);
            when(ImageSource.create(any(String.class),any())).thenReturn(imageSource);
            ImageInfo imageInfo = mock(ImageInfo.class);
            when(imageSource.getImageInfo()).thenReturn(imageInfo);
            Size size = mock(Size.class);
            Whitebox.setInternalState(imageInfo,"size",size);
            options.desiredSize = size;
            PixelMap pixelMap = mock(PixelMap.class);
            PixelMap p = ImageUtil.decodeSampledBitmapFromFile("D:/bjc/text/",2,2);
            Assert.assertEquals(null,p);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteTempFile() {
        String path = "d:/abc/cdd/";
        File file = new File(path);
        file.mkdirs();
        ImageUtil.deleteTempFile(path);
        boolean s = file.exists();
        Assert.assertEquals(false,s);
    }
}