# CompressHelper

## **Functions overview**

​	图片压缩：主要通过尺寸压缩和质量压缩，以达到清晰度最优

## Effect picture<br>

![输入图片说明](https://images.gitee.com/uploads/images/2021/0707/183438_98110ce7_8230582.gif "SVID_20210707_182743_1.gif")

## Getting Started

#####
Solution 1: Remote warehouse import project
```
implementation ‘com.gitee.baijuncheng-open-source:compresshelper:1.0.0’
```

Solution 2：Add compresshelper to your project

1.Copy the compresshelper/entry folder to the project directory

2.Modify settings.gradle under the project and add the dependency on this module as follows:
```
include ':entry', ':compresshelper'
```

3. Introduce the dependency of the module under the project. Taking the entry module as an example, you need to modify the build.gradle file under the entry module to add the dependency:
```
implementation project(':compresshelper')
```

Solution 3: local use of har package integration
1. Compile this project, copy the har package generated in the build directory of the compresshelper/entry folder to the project lib folder:
directory：\CompressHelper_master\compresshelper\build\outputs\har\debug\compresshelper-debug.har
2. Add the following code in the entry's gradle
```
implementation fileTree(dir:'libs', include: ['*.jar','*.har'])
```

​	More configuration can be found in the Project.Usage

- Used within Ability:

```java
newFile = CompressHelper.getDefault(getApplicationContext()).compressToFile(oldFile);
```
- You can also customize compression

```java
newFile = new CompressHelper.Builder(this)
       .setMaxWidth(720)  // 默认最大宽度为720
        .setMaxHeight(960) // 默认最大高度为960
        .setQuality(80)    // 默认压缩质量为80
        .setCompressFormat(ImageFormat.ComponentType.JPEG) // 设置默认压缩为jpg格式
        .setFileName(yourFileName) // 设置你的文件名
        .setDestinationDirectoryPath(
                Environment.DIRECTORY_PICTURES)
        .build()
        .compressToFile(oldFile);
```

## 

## Fealture

- Supports compression of images of different formats, sizes and memory sizes <br>

  

## License

 Copyright 2017 nanchen(刘世麟)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

```
  http://www.apache.org/licenses/LICENSE-2.0
```

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.