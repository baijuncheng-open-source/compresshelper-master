package com.nanchen.compressimage;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Image;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class ImageItemProvider extends BaseItemProvider {

    private Context mContext;
    private List<ItemInfo> images = new ArrayList<>();


    public ImageItemProvider(Context context, List<ItemInfo> data) {
        mContext = context;
        images.addAll(data);
    }

    @Override
    public int getCount() {

        return images.size();
    }

    @Override
    public Object getItem(int i) {
        return images.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component root = null;
        if (component == null) {
            root = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_image_item_layout, null, false);
        } else {
            root = component;
        }

        Image im = (Image) root.findComponentById(ResourceTable.Id_im_item);
        im.setPixelMap(images.get(i).pixelMap);
        return root;
    }
}
