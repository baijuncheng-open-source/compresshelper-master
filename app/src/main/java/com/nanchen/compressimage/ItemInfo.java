package com.nanchen.compressimage;

import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

public class ItemInfo {
    public Uri uri;
    public PixelMap pixelMap;

    public ItemInfo(Uri uri, PixelMap pixelMap) {
        this.uri = uri;
        this.pixelMap = pixelMap;
    }
}
