package com.nanchen.compressimage.slice;

import com.nanchen.compressimage.ImageItemProvider;
import com.nanchen.compressimage.ItemInfo;
import com.nanchen.compressimage.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.data.resultset.ResultSet;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.nanchen.compressimage.MainAbility.RESULT_OK;

public class ImageListAbilitySlice extends AbilitySlice {
    private ListContainer ls_image;
    private List<ItemInfo> images = new ArrayList<>();
    private final int loadCount = 10;
    ImageItemProvider imageItemProvider;
    EventHandler handler = new EventHandler(EventRunner.getMainEventRunner()) {
        @Override
        protected void processEvent(InnerEvent event) {
            imageItemProvider = new ImageItemProvider(ImageListAbilitySlice.this, images);
            ls_image.setItemProvider(imageItemProvider);
        }
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_image_list);
        ls_image = (ListContainer) findComponentById(ResourceTable.Id_ls_image);
        ls_image.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                Uri uri = images.get(i).uri;
                Intent intent1 = new Intent();
                intent1.setParam("uri", uri.toString());
                getAbility().setResult(RESULT_OK, intent1);
                terminateAbility();
            }
        });
        getImage();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    public void getImage() {
        DataAbilityHelper helper = DataAbilityHelper.creator(this);
        try {
            // columns为null，查询记录所有字段，当前例子表示查询id字段
            ResultSet result = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, null, null);
            if (result == null) {
                return;
            }
            int index = 0;
            while (result.goToNextRow() && index < loadCount) {
                int id = result.getInt(result.getColumnIndexForName(AVStorage.Images.Media.ID));
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
                try {
                    FileDescriptor fd = helper.openFile(uri, "r");
                    ImageSource imageSource = ImageSource.create(fd, null);
                    PixelMap pixelMap = imageSource.createPixelmap(null);
                    ItemInfo itemInfo = new ItemInfo(uri, pixelMap);
                    images.add(itemInfo);
                    index++;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            result.close();
            handler.sendEvent(100);
        } catch (DataAbilityRemoteException e) {
            // ...
        }
    }
}
