package com.nanchen.compressimage;

import com.nanchen.compressimage.slice.ImageListAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ImageListAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ImageListAbilitySlice.class.getName());
    }
}
